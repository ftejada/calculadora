# Programa para simular una calculadora
def sumar(num1, num2):
    return num1 + num2

def restar(num1, num2):
    return num1 - num2

print("Suma 1: 1 + 2 =", sumar(1,2))
print("Suma 2: 3 + 4 =", sumar(3,4))
print("Resta 1: 6 - 5 =", restar(6,5))
print("Resta 2: 8 - 7 =", restar(8,7))